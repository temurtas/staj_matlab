function mbd = spherical_mirror_aberr(fn,D)

% fn is the �f-number� of a concave spherical mirror
% D is its diameter in millimeters
% mbd is the mean blur diameter in millimeters.
% The f-number equals the focal length f of the mirror
% divided by its diameter. Ideally, all the rays of light from 
% a distant object, illustrated by the parallel lines in the
% figure, would reflect off the mirror and
% then converge to a single focal point. The
%magnified view shows what actually happens.
% The light striking a vertical plane at a distance f from 
% the mirror is spread over a circular disk.

% The light from the center of the mirror strikes the center of
% the disk, but light arriving from a point a
% distance x from the center of the mirror
% strikes the plane on a circle whose diameter d is equal to 
% 2ftan(2teta)(1/cos(teta) -1) where teta=sin^-1(x/2f)

delta_x=0.01

x=0:delta_x:D/2



teta=asin(x/(2*fn))

d= 2*fn*tan(2*teta).*(sec(teta) -1)

mbd=(8*delta_x/D^2)*sum(sum(x'*d))

end

