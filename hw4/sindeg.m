function [S d]=sindeg(deg)
[x y] = size(deg)
S1=deg2rad(deg)
S= sin(S1)
d1=sum(S)
e=sum(d1)
d=e/(x*y)

end