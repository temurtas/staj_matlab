function orms=odd_rms(nn)

x=1:nn
xx=1:2:1
x2=x.^2
x3=sum(x2)/((nn+1)/2)
orms=sqrt(x3)


end