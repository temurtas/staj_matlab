function ss=functop(M)



s=M(1,1)
s2=M(end,1)
s3=M(1,end)
s4=M(end,end)

ss=s+s2+s3+s4

end