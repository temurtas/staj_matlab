function ps= peri_sum(A)

s1=A(1,:)+A(end,:)
s2=A(:,1)+A(:,end)
ps=sum(s1)+sum(s2)-A(1,end)-A(end,end)-A(end,1)-A(1,1)
end