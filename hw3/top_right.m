function NN = top_right(N,n)
[m,e]=size(N);
NN=N(1:n,e-n+1:e);

end