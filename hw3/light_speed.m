function [A,B]=light_speed(M)

A=M./(300000*60)
B=M./1609

end
